package TestScript;

import java.io.IOException;


import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import BrowserFactory.BaseClass;
import POM_Pages.Cartpage;
import POM_Pages.Chcekoutpage;
import POM_Pages.Homepage;
import POM_Pages.LoginPage;
import POM_Pages.Logoutpage;
import POM_Pages.OverviewPage;
import utility.ExcelFileReader;
import utility.Screenshot;


public class TestScript1 {
	
	WebDriver driver;
	BaseClass Base;
	LoginPage login;
	Homepage Home;
	Cartpage Cart;
	Chcekoutpage Chcek;
	OverviewPage Overview;
	Logoutpage logout;
	String TCIDs;
	Screenshot screen;
	
	@BeforeClass
	public void OpenBrowser() throws InterruptedException{
		Base = new BaseClass();
		driver = Base.OpenChromeBrowser();
	}
	@BeforeMethod
	public void Login_To_Swag_Labz() throws InterruptedException {
		login = new LoginPage(driver);
		Thread.sleep(1000);
		login.SetuserName("standard_");
		Thread.sleep(1000);
		login.SetPassword("secret_sauce");
		Thread.sleep(1000);
		login.ClickOnLoginBtn();
		Thread.sleep(1000);	
	}
	@DataProvider
	public Object[][] getTestData() throws EncryptedDocumentException, InvalidFormatException, IOException {
		ExcelFileReader E = new ExcelFileReader();
		Object[][] data = E.getData();
		return data;
	}
	@Test(dataProvider = "getTestData")
	public void Buy_A_Product(String TCID, String Fname, String Lname, String Pincode) throws InterruptedException {
		try {
		TCIDs = TCID;
		Home = new Homepage(driver);
		Cart = new Cartpage(driver);
		Chcek = new Chcekoutpage(driver);
		Overview = new OverviewPage(driver);
		Assert.assertTrue(Home.Verifylogo());
		Thread.sleep(2000);
		Home.Click_On_Add_To_Cart_Button();
		Thread.sleep(2000);
		Home.Click_On_Cart_Icon();
		Thread.sleep(2000);
		Cart.Get_Product_Name();
		Thread.sleep(2000);
		Cart.Get_Product_Prize();
		Thread.sleep(2000);
		Cart.Click_On_Checkout_Button();
		Thread.sleep(2000);
		Chcek.Enter_Fname(Fname);
		Thread.sleep(2000);
		Chcek.Enter_Lname(Lname);
		Thread.sleep(2000);
		Chcek.Enter_Pincode(Pincode);
		Thread.sleep(2000);
		Chcek.Click_On_Continue_Button();
		Thread.sleep(2000);
		Overview.Click_On_Finish_Button();
		Thread.sleep(2000);
		}
		catch(Exception e) {
			e.printStackTrace();		
		}	
	}
	@AfterMethod
	public void Logout_Make_my_trip(ITestResult result) throws InterruptedException {
		if(ITestResult.FAILURE == result.getStatus()) {
			screen = new Screenshot();
			screen.TakeScreenshot(driver,TCIDs);
		}
		logout = new Logoutpage(driver);
		logout.ClickOnSideMenu();
		Thread.sleep(2000);
		logout.ClickOnLogoutBtn();
		Thread.sleep(2000);
		
	}	
	@AfterClass
	public void CloseBrowser() {
		Base = new BaseClass();
		Base.CloseChromeBrowser(driver);
	}
		
}
