package POM_Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Cartpage {
	
	@FindBy(xpath = "//div[text()= \"Sauce Labs Backpack\"]")private WebElement product_name;
	@FindBy(xpath = "//div[@class=\"inventory_item_price\"]")private WebElement product_price;
	@FindBy(xpath = "//button[@id=\"checkout\"]")private WebElement checkout_btn;
	@FindBy(xpath = "//span[text()= \"Checkout: Your Information\"]")private WebElement checkout_page;
	
	public Cartpage(WebDriver driver) {
		PageFactory.initElements(driver, this);	
	}
	public void Get_Product_Name() {
		product_name.getText();
	}
	public void Get_Product_Prize() {
		product_price.getText();
	}
	public void Click_On_Checkout_Button() {
		checkout_btn.click();
		boolean result = checkout_page.isDisplayed();
		if(result == true) {
			System.out.println("Personal Information Page should displayed on the screen");
		}else {
			System.out.println("Personal details page should not be displayed");
		}
	}

}
