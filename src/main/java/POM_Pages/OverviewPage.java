package POM_Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OverviewPage {
	
	@FindBy(xpath = "//button[@id=\"finish\"]")private WebElement Finish;
	@FindBy(xpath = "//h2[text() = 'Thank you for your order!']")private WebElement Thanku_msg;
	@FindBy(xpath = "//button[text()= \"Open Menu\"]")private WebElement side_menu;
	
	public OverviewPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	public void Click_On_Finish_Button() {
		Finish.click();
		boolean result = Thanku_msg.isDisplayed();
		if(result == true) {
			System.out.println("User should be placed order successfully");
		}else {
			System.out.println("User should not placed order successfully");
		}
	}
	

}
