package POM_Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utility.PropertyFileReader;

public class LoginPage {
	
	@FindBy(xpath = "//input[@id = \"user-name\"]")private WebElement user_name;
	@FindBy(xpath = "//input[@id = \"password\"]")private WebElement password;
	@FindBy(xpath = "//input[@id = \"login-button\"]")private WebElement login;
	
	
	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver,this);
	}
	PropertyFileReader p = new PropertyFileReader();
	
	
	public void  SetuserName(String Username) {
		user_name.sendKeys(Username);	
	}
	public void SetPassword(String Password) {
		password.sendKeys(Password);
	}
	public void ClickOnLoginBtn() {
		login.click();
	}

}
