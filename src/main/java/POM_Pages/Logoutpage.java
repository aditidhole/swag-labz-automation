package POM_Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Logoutpage {
	
	@FindBy(xpath = "//button[@id=\"react-burger-menu-btn\"]")private WebElement side_menu;
	@FindBy(xpath = "//a[@id=\"logout_sidebar_link\"]")private WebElement logout;
	
	public Logoutpage(WebDriver driver) {
		PageFactory.initElements(driver, this);	
	}
	public void ClickOnSideMenu() {
		side_menu.click();
	}
	public void ClickOnLogoutBtn() {
		logout.click();
	}

}
