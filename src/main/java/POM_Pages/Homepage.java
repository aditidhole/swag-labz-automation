package POM_Pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class Homepage {
	
	@FindBy(xpath = "//div[text() = 'Swag Labs']")private WebElement logo;
	@FindBy(xpath = "//button[@id=\"add-to-cart-sauce-labs-backpack\"]")private WebElement Add_to_cart;
	@FindBy(xpath = "//div[@id=\"shopping_cart_container\"]")private WebElement Cart_icon;
	@FindBy(xpath = "//span[text() = \"Your Cart\"]")private WebElement your_cart;
	
	public Homepage(WebDriver driver) {
		PageFactory.initElements(driver, this);	
	}
	public boolean Verifylogo() {
		try {
			boolean result = logo.isDisplayed();
			if(result == true) {
				System.out.println("Homepage is present & user login successfully");
			}else {
				System.out.println("user is not login  Successfully");
			}
	        return result;
		} catch (NoSuchElementException e) {
	        return false;
	    }
	}
	public void Click_On_Add_To_Cart_Button() {
		Add_to_cart.click();
	}
	public void Click_On_Cart_Icon(){
		Cart_icon.click();
		boolean result = your_cart.isDisplayed();		
		if(result == true) {
			System.out.println("Cart Page Should Displayed on the screen");
		}else {
			System.out.println("Cart page should not dosplayed on the screen");
        }
	
	}


}
