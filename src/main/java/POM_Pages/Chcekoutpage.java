package POM_Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Chcekoutpage {
	
	@FindBy(xpath = "//input[@id=\"first-name\"]")private WebElement first_name;
	@FindBy(xpath = "//input[@id=\"last-name\"]")private WebElement last_name;
	@FindBy(xpath = "//input[@id=\"postal-code\"]")private WebElement pincode;
	@FindBy(xpath = "//input[@id=\"continue\"]")private WebElement continue_btn;
	@FindBy(xpath = "//span[text()= \"Checkout: Overview\"]")private WebElement overviewpage;
	
	public Chcekoutpage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	public void Enter_Fname(String Fname) {
		first_name.sendKeys(Fname);
	}
	public void Enter_Lname(String Lname) {
		last_name.sendKeys(Lname);
	}
	public void Enter_Pincode(String Pincode) {
		pincode.sendKeys(Pincode);
	}
	public void Click_On_Continue_Button() {
		continue_btn.click();
		boolean result = overviewpage.isDisplayed();
		if(result == true) {
			System.out.println("Overview page should displayed on the screen");
		}else {
			System.out.println("Overview page should not be displayed");
		}
	}

}
