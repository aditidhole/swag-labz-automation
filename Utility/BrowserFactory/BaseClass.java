package BrowserFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import utility.PropertyFileReader;


public class BaseClass {
	
	public WebDriver OpenChromeBrowser() {
		PropertyFileReader p = new PropertyFileReader();
		
		System.setProperty("webdriver.chrome.driver",p.getChromePath());
		//Disable Chrome Notifications
		
		ChromeOptions opt = new ChromeOptions();
		opt.addArguments("--disable-notifications");
		
		WebDriver driver = new ChromeDriver(opt);
		driver.get(p.getURL());
//		driver.manage().window().maximize();
		return driver;
		
	}
	public void CloseChromeBrowser(WebDriver driver) {
		driver.close();
	}

}
