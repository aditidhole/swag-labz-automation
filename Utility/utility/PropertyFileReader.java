package utility;

import java.io.FileInputStream;
import java.util.Properties;

public class PropertyFileReader {
	
Properties pro;
	
	public PropertyFileReader(){
		try {
		    FileInputStream file = new FileInputStream("C:\\Users\\91724\\eclipse-workspace\\SwagLabz\\Resources\\Config.properties");
		    pro = new Properties();
		    pro.load(file);
		}
		catch(Exception E) {
			E.printStackTrace();
		}
	}
    public String getUsername() {
    	String str = pro.getProperty("Username");
    	return str;
    }

    public String getPassword() {
    	String str2 = pro.getProperty("Password");
    	return str2;   
    }
    public String getURL() {
    	String str3 = pro.getProperty("Url");
    	return str3;
    }
    public String getChromePath() {
    	String str4 = pro.getProperty("ChromeDriverPath");
		return str4;
    }

}
