package utility;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;


public class ExcelFileReader {
	
	public Object[][] getData() throws EncryptedDocumentException, IOException, InvalidFormatException {
		FileInputStream File = new FileInputStream("C:\\Users\\91724\\Desktop\\Ecommerce- Swag Labs.xls");
		Sheet SH = WorkbookFactory.create(File).getSheet("Sheet1");
		Object[][] data = new Object[SH.getLastRowNum()][SH.getRow(0).getLastCellNum()];
		for(int i = 0; i<SH.getLastRowNum(); i++) {
			for(int j = 0; j<SH.getRow(0).getLastCellNum(); j++) {
				data[i][j] = SH.getRow(i+1).getCell(j).toString();	
			}
		}
	return data;	
	}
//	public static void main(String[] args) throws EncryptedDocumentException, IOException, InvalidFormatException{
//		ExcelFileReader re = new ExcelFileReader();
//		System.out.println(re.getData());
//		for(Object[] r: re.getData()) {
//			for(Object c: r) {
//				System.out.println(c);
//			}
//			
//		}"C:\Users\91724\Desktop\Ecommerce- Swag Labs.xlsx"
	//}


}
