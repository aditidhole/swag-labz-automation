package utility;

import java.io.File;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;

public class Screenshot {
	
	public void TakeScreenshot(WebDriver driver, String TCID){
		try{File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		File Desc = new File("C:\\Users\\91724\\eclipse-workspace\\SwagLabz\\Screenshot\\"+TCID+ ".jpg");
		FileHandler.copy(src,Desc);
		}catch(Exception e) {
	    	e.printStackTrace();
	    }
			
	}

}
